import h5py
import numpy as np
from pylab import *
import os
import cartopy.crs as ccrs
import cartopy
import cartopy.feature as cfeature
from matplotlib.colors import LightSource
from scipy.interpolate import RegularGridInterpolator 
from matplotlib.colors import LinearSegmentedColormap
import shapefile as shp 
import matplotlib.path as mplp
from  shapely.geometry import *
from netCDF4 import Dataset
import cartopy.io.shapereader as shpreader
import time
from matplotlib.image import imread
from scipy.ndimage import gaussian_filter 


##########################################################################################################
# This library is for mapping of gridded data such as OMI NO2. It provides many options so users have 
# flexibility with the maps they make. Below are the options for users. 
#
# fig, ax : sublot information for plotting map, allows user to make multiple subplots with same library
#
# data: Gridded Level 3 data for mapping. Should be 2x2 degree array that covers region in grid_extent, 
# which defaults to global 
# 
# data_min, data_max: Min/Max of colorbar for data being mapped (Defaults to 0 and 20)
#
# grid_extent: lat/lon boundaries of input data (Default assumes global data)
#
# map_extent: lat/lon boundaries of region user wants mapped (Default assumes global map)
#
# colorbar: String for the colormap of choice which refers to RGB text file in Colorbars/ directory. 
# If user inputs 'custom', they can provide keyword to cmap for user defined colormap (default is NO2 colorbar)
#
# cmap: If colorbar keyword is "custom", user should provide python colormap 
#
# cb_label, cb_label: Label for bottom of colorbar and top of colorbar respectively. (Default is no colorbar label)
# 
# blue_marble: boolean flag as to whether user wants MODIS blue marble plotted in background where no data is available 
# (defulat is True)
#
# modis_image: filename of desired background blue marble image. (Default is general blue marble from MODIS)
#
# plot_ocean: boolean to say whether data should be mapped over oceans. If False, Oceans will be colored as grey (Default
# is False)
# 
# topo: Whether user wants topography data mapped below data (Default is True)
#
# res: Resolution of topography data that user wants mapps. 'high' refers to 1 minute topo data, while 'low' refers to 
# 2 minute topo data (default is high)
# 
# provinces: Boolean flag that defines whether province boundaries mapped (Default is True)
#
# roads: Boolean flag that defines whether roads are mapped (Default is True)
#
# mask: Mask used for mapping data. For example, user can supply mask for specfic country. As of now, should be same 
# lat/lon grid as ETOPO data. (Default is None)
#
# scale: Scale default size of colorbar labels (Default is 1.0)
#
# show_cb: Boolean flag which defines whether colorbar is plotted on map (Default is False)
#
# ticklabel_perc: Boolean flag which defines if ticklabels should have % at the end (Default is False)
#
# diff: Boolean flag which defines if data shows difference of two datasets (Default is False)
#
# cbar_ax: Array that defines location of colorbar. [x-position, y-position, width, height] (Default is [])


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

#Not currently implemented, but provided as optional function for user. Pulls in shapefile information 
#for all countries. Original use is for masking countries to only show specific countries, but doing 
#such is slow, so instead a country mask (CountryMask.h5) file was created and can be implemented if wanted
def get_shapes():
    shapes = {}
    sf = shp.Reader("ne_10m_admin_0_countries/ne_10m_admin_0_countries.shp")
    for shape in sf.shapeRecords():

        if shape.record[3] not in shapes:
            shapes[shape.record[3]] = []
        parts = shape.shape.parts
        cur_points = np.array(shape.shape.points)
        for i in range(len(parts)-1):
            shapes[shape.record[3]].append(cur_points[parts[i]:parts[i+1]])
        
    return shapes

#Not currently used, but can be implemented to create a mask for specific polygons such as country shapefiles
def outline_to_mask(line, x, y):
    """Create mask from outline contour

    Parameters
    ----------
    line: array-like (N, 2)
    x, y: 1-D grid coordinates (input for meshgrid)

    Returns
    -------
    mask : 2-D boolean array (True inside)
    """
    import matplotlib.path as mplp
    mpath = mplp.Path(line)
    points = np.array((x.flatten(), y.flatten())).T
    mask = mpath.contains_points(points).reshape(x.shape)
    return mask

def map_data(fig,ax,data,data_min=0,data_max=20,grid_extent=[-180,180,-90,90],map_extent=[-180,180,-90,90],colorbar='NO2',cmap='',cb_label='',cb2_label='',blue_marble=True,modis_image='BlueMarble/Blue.png',plot_ocean=False,topo=True,provinces=True,res='high',roads=True,mask=None,scale=1.,show_cb=False,ticklabel_perc=False,diff=False,cbar_ax=[]):

    start_time = time.time()
    print('Starting Map')
    print("--- %s seconds ---" % (time.time() - start_time))

    data_x_len = len(data[:,0])
    data_y_len = len(data[0,:])
    
    data_lat = np.linspace(grid_extent[2],grid_extent[3],data_x_len)
    data_lon = np.linspace(grid_extent[0],grid_extent[1],data_y_len)

    if blue_marble:
        #Making MODIS Blue Marble the background image where there is no data available
        #MODIS Blue Marble images are for 2004 and are available for each month, simply
        #change your input modis_image to blue modis filename that you want for background
        img = imread(modis_image)

        ax.imshow(img,origin='upper',transform=ccrs.PlateCarree(),interpolation='nearest',extent=[-180,180,-90,90],regrid_shape=2000)
        
        #Deleting Blue marble image to improve memory usage 
        del img 
        print('Blue Marble Mapped')
        print("--- %s seconds ---" % (time.time() - start_time))

    if topo:

        if res == 'high':
            #Grabbing high resolution, 1 minute, ETOPO data which will be plotted as layer under NO2. This can possibly be replaced using
            #cartopy SRTM libraries, https://scitools.org.uk/cartopy/docs/v0.14/examples/srtm_shading.html, just need to work out the masking
            #as blue marble should be shown where there is no NO2 data, but then where there is data, terrain should be on top of blue marble
            #Was places in main library to be called everytime so memory can be released when not being used, passing in from driver may improve
            #speed 
            a = Dataset('ETOPO1_Ice_g_gdal.grd','r')
            etopo_x = a['dimension'][1]
            etopo_y = a['dimension'][0]
            etopo_hgt = a['z'][:].reshape(etopo_x,etopo_y)
            etopo_hgt = np.array(etopo_hgt[::-1,:],dtype=np.float32)/1000.
            highres_lat = np.array(np.repeat(np.linspace(-90,90,etopo_x)[:,np.newaxis],etopo_y,-1),dtype=np.float32)
            highres_lon = np.array(np.repeat(np.linspace(-180,180,etopo_y)[np.newaxis,:],etopo_x,0),dtype=np.float32)
            a.close()
        else:
            #Optional loading in of lower resolution 2 minute ETOPO terrain map
            etopo_filename = 'ETOPO2v2c.h5'
            d = h5py.File(etopo_filename)
            etopo_lat = d['/Data Fields/Latitude'][:]
            etopo_lon = d['/Data Fields/Longitude'][:]
            highres_lat =  np.repeat(etopo_lat[:,np.newaxis],len(etopo_lon),axis=-1)
            highres_lon =  np.repeat(etopo_lon[np.newaxis,:],len(etopo_lat),axis=0)
            etopo_hgt = d['/Data Fields/TerrainHeight'][:]/1000.
            etopo_x = len(etopo_hgt[:,0])
            etopo_y = len(etopo_hgt[0,:])


        #Mask topo data outside of map boundaries so only map region is actually mapped 
        highres_lat[(highres_lat < (map_extent[2]-5)) | (highres_lat > (map_extent[3]+5))] = np.nan
        highres_lon[(highres_lon < (map_extent[0]-5)) | (highres_lon > (map_extent[1]+5))] = np.nan

        print('Topo Data Read')
        print("--- %s seconds ---" % (time.time() - start_time))

        #Interpolating input data to etopo grid resolution. This is so we can plot the smoothed data at high resolution and also 
        #mask the ETOPO data so it is only mapped where there is good input data. Imshow interpolation does not make a smooth 
        #enough map without the higher resolution data
        fn= RegularGridInterpolator((data_lat,data_lon),data,bounds_error=False,fill_value= np.array([np.nan],dtype=np.float32)[0])
        highres_data = fn((highres_lat.flatten(),highres_lon.flatten())).reshape(etopo_x,etopo_y)

        #Masking topo data where there is no good data so that Blue Marble shows through 
        etopo_hgt[np.isnan(highres_data)] = np.nan

        print('High Res Smoothing finished')
        print("--- %s seconds ---" % (time.time() - start_time))

        #Using Matplotlib LightSource library to make a mesh of topography shadowing where azdeg is your azimuthal angle from 
        #north and alt degree is your altitude in the sky. Vert_exag is the magnitude of the shadowing in the mesh
        ls = LightSource(azdeg=315, altdeg=45)

        shade_topo = ls.hillshade(etopo_hgt, vert_exag=20)

        #Deleting etopo variables to limit memory overhead 
        del etopo_hgt
        del highres_lat
        del highres_lon 

        #Mapping the topography given the shadow mesh from above, regrid shape used for high resolution mapping as non-PlateCarree 
        #projections (even if PlateCarree is given as transform) produce pixelated images without regrid_shape, possible cartopy bug?
        ax.imshow(shade_topo, extent=[-180,180,-90,90],cmap='gray',alpha=1.,interpolation='nearest',transform=ccrs.PlateCarree(),rasterized=False,regrid_shape=3000)

        print('Topo Data Mapped')
        print("--- %s seconds ---" % (time.time() - start_time))
    else:

        highres_lat = np.repeat(np.linspace(-90,90,10801)[:,np.newaxis],21601,axis=-1)
        highres_lon = np.repeat(np.linspace(-180,180,21601)[np.newaxis,:],10801,axis=0)

        #Interpolating input to high resolution lat/lon if topo is not being mapped. This is so we can plot the smoothed data at 
        #high resolution and also mask the ETOPO data so it is only mapped where there is good input data 
        fn= RegularGridInterpolator((data_lat,data_lon),data,bounds_error=False,fill_value= np.array([np.nan],dtype=np.float32)[0])
        highres_data = fn((highres_lat.flatten(),highres_lon.flatten())).reshape(10801,21601)

        #Deleting high res lat/lon to limit memory overhead 
        del highres_lat
        del highres_lon 

    #Can provide an input mask such as a country mask on the ETOPO grid to only plot data for this mask 
    if mask is None:
        print('')
    else:
        highres_data[~mask] = np.nan


    #Making colorbar, have preloaded RGB text files for NO2 and NO2 difference colorbars based on previously use colorbars. More 
    #RGB text files likely to be added for other colorbars int he future 
    if colorbar != 'custom':
        colors = (np.loadtxt('Colorbars/'+colorbar+'_Colors.txt')/255.)

        cmap = LinearSegmentedColormap.from_list('Data',colors,N=200)
        cmap.set_over(tuple(colors[-1]))
        cmap.set_under(tuple(colors[0]))
    #Can also provide custom python colormap 
    elif colorbar == 'custom':
        cmap = cmap
    
    else:
        print('Colorbar provided is not NO2 or custom, defaulting to NO2')
        colors = np.loadtxt('NO2_Colors.txt')/255.
        cmap = LinearSegmentedColormap.from_list('Data',colors,N=200)
        cmap.set_over(tuple(colors[-1]))
        cmap.set_under(tuple(colors[0]))

    #Mapping the gridded data using imshow, using 90% transparency so that terrain comes through
    im = ax.imshow(highres_data,extent=[-180,180,-90,90],vmin=data_min, vmax=data_max, transform=ccrs.PlateCarree(),alpha=0.9,cmap=cmap,interpolation='nearest',rasterized=False,regrid_shape=2500)

    #Deleting high resolution data to limit memory overhead 
    del highres_data

    print('High Res Data Mapped')
    print("--- %s seconds ---" % (time.time() - start_time))

    #Mapping data as NaNs for use in colorbar. With python2.7, the transpareny above makes the colorbar a bit off, so 
    #a "fake" plot as below simplifies the colorbar and makes it look clean 
    im = ax.imshow([[np.nan,np.nan],[np.nan,np.nan]],extent=[-180,180,-90,90],vmin=data_min, vmax=data_max, transform=ccrs.PlateCarree(),alpha=1.,cmap=cmap,interpolation='nearest',rasterized=False)

    #Placing the colorbar on the map itself if show_cb is True
    #A few options are used here...drawedges removes random white lines in colorbar, extend adds the triangles on the end 
    #of the colorbar 
    if plot_ocean:
        cb_color = 'k'
    else:
        cb_color='white'

    if show_cb:

        cax = fig.add_axes(cbar_ax)

        cb = fig.colorbar(im,cax=cax,cmap=cmap,orientation='horizontal',drawedges=False,extend='both')
        cb.set_ticks([data_min,(data_max+data_min)/2,data_max])
        if ticklabel_perc:
            cb.set_ticklabels([str(data_min)+'%','0',str(data_max)+'%'])

        cb.set_label(cb_label,fontsize=13*scale,color=cb_color)
        cb.ax.xaxis.set_tick_params(color=cb_color)
        cb.ax.tick_params(labelsize=12*scale)
        cb.outline.set_edgecolor(cb_color)
        plt.setp(plt.getp(cb.ax.axes,'xticklabels'),color=cb_color)

        #Making a second colorbar to put Low/High labels on top of colorbar 
        cax2 = cax.twiny()
        cb2 = fig.colorbar(im,cax=cax2,cmap=cmap,orientation='horizontal',drawedges=False,extend='both')
        cax2.xaxis.set_ticks_position('top')
        cb2.set_ticks([data_min,(data_max+data_min)/2,data_max])
        if diff == True:
            cb2.set_ticklabels(['Below','','Above'])
        else:
            cb2.set_ticklabels(['Lower','','Higher'])
        cb2.ax.tick_params(labelsize=12*scale)
        cb2.ax.xaxis.set_tick_params(color=cb_color)
        cb2.outline.set_edgecolor(cb_color)
        
        cb2.ax.set_title(cb2_label,fontsize=14*scale,color=cb_color,fontweight='bold',pad=1.2*scale)
        plt.setp(plt.getp(cb2.ax.axes,'xticklabels'),color=cb_color)

    

    #Putting some physical boundaries on the map 
    if provinces:
        states_provinces = cfeature.NaturalEarthFeature(category='cultural',name='admin_1_states_provinces_lines',scale='10m',facecolor='none')
        ax.add_feature(states_provinces, edgecolor='grey')

    countries = cfeature.NaturalEarthFeature(category='cultural',name='admin_0_countries',scale='10m',facecolor='none',lw=0.4)
    ax.add_feature(countries,edgecolor='k')

    lakes = cfeature.NaturalEarthFeature(category='physical',name='lakes',scale='10m',facecolor='dimgrey',lw=0.15)
    ax.add_feature(lakes,edgecolor='grey')

    
    if roads:
        shpfilename = shpreader.natural_earth(resolution='10m',category='cultural',name='roads')
        reader = shpreader.Reader(shpfilename)
        roads = reader.records()
        for road in roads:
            
            if (road.attributes['sov_a3'] == 'USA') &(road.attributes['level'] == 'Interstate') & (road.attributes['type']== 'Major Highway'):

                ax.add_geometries([road.geometry],ccrs.PlateCarree(),edgecolor='grey',facecolor='None',lw=0.3,alpha=0.7)

            if (road.attributes['sov_a3'] != 'USA') &(road.attributes['expressway'] == 1):
                ax.add_geometries([road.geometry],ccrs.PlateCarree(),edgecolor='grey',facecolor='None',lw=0.3,alpha=0.7)

    if not plot_ocean:
        water_50m = cfeature.NaturalEarthFeature('physical', 'ocean', '10m',edgecolor='face',facecolor='dimgrey',lw=0.15)
        ax.add_feature(water_50m)

    ax.coastlines(resolution='10m',lw=0.5)
    print('map layers added')
    print("--- %s seconds ---" % (time.time() - start_time))

    ax.set_extent(map_extent,crs=ccrs.PlateCarree())
    print('extent set')

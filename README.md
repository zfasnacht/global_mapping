# Global Mapping

This is a python library for mapping gridded datasets topography and MODIS Blue Marble underlayed below the mapped data

The library itself is called global_mapping. There is a driver in this repository called map_no2_subregions.py which pulls 
in user inputs from a control file and creates several global NO2 maps including 2020 March NO2, 2015-2019 March NO2 (aka 
baseline), and the difference between 2020 March NO2 and the baseline NO2.

***NOTE: ETOPO datasets are too large for github repository, so they need to be unzipped when downloaded

The driver can be used with a simple call:

*python map_no2_subregions.py month output_map_directory input_controlfile*

where:

month is an integer for the desired month (data only included for March)
output_map_dirctory is the directory for maps to be stored
input_controlfile is a control file with user selected input features. The columns in the control file are:

*Name, LonMin, LonMax, LatMin, LatMax, LowerBound, UpperBound, MinDiff, MaxDiff, Topo, PlotOcean, Roads,*
*Provinces, cbar_x, cbar_y, cbar_width, cbar_height, scale*

where:

Name is name of map region
LonMin/LonMax/LatMin/LatMax are bounds of map
LowerBound/UpperBound are lower and upper bounds of data (for absolute data, not difference)
MinDiff/MaxDiff are lower and upper bounds of difference between two different datasets (here would be 2020 vs baseline)
Topo is boolean flag as to whether topography is mapped underneath data
PlotOcean is boolean flag as to whether data is plotted over oceans
Roads is boolean flag as to whether roads are mapped
Provinces is boolean flag as to whether province boundaries are mapped
cbar_x, cbar_y, cbar_width, cbar_height are location of colorbar
scale is optional argument that scales font size of colorbar text 

The library itself can be called like below:

*from global_mapping import*

*map_data(fig,ax,data,data_min=0,data_max=20,grid_extent=[-180,180,-90,90],map_extent=[-180,180,-90,90],*
*colorbar='NO2',cmap='',cb_label='',cb2_label='',blue_marble=True,modis_image='BlueMarble/Blue',*
*plot_ocean=False,topo=True,provinces=True,res='high',roads=True,mask=None,scale=1.,show_cb=False,*
*ticklabel_perc=False,diff=False,cbar_ax=[])*


where the optional inputs to the library are:

fig, ax : sublot object for plotting map, allows user to make multiple subplots with same library

data: Gridded Level 3 data for mapping. Should be 2x2 degree array that covers region in grid_extent,
which defaults to global

data_min, data_max: Min/Max of colorbar for data being mapped (Defaults to 0 and 20)

grid_extent: lat/lon boundaries of input data (Default assumes global data)

map_extent: lat/lon boundaries of region user wants mapped (Default assumes global map)

colorbar: String for the colormap of choice which refers to RGB text file in Colorbars/ directory.
If user inputs 'custom', they can provide keyword to cmap for user defined colormap (default is NO2 colorbar)

cmap: If colorbar keyword is "custom", user should provide python colormap

cb_label, cb_label: Label for bottom of colorbar and top of colorbar respectively. (Default is no colorbar label)

blue_marble: boolean flag as to whether user wants MODIS blue marble plotted in background where no data is available
(defulat is True)

modis_image: filename of desired background blue marble image. (Default is general blue marble from MODIS)

plot_ocean: boolean to say whether data should be mapped over oceans. If False, Oceans will be colored as grey (Default
is False)

topo: Whether user wants topography data mapped below data (Default is True)

res: Resolution of topography data that user wants mapps. 'high' refers to 1 minute topo data, while 'low' refers to
2 minute topo data (default is high)

provinces: Boolean flag that defines whether province boundaries mapped (Default is True)

roads: Boolean flag that defines whether roads are mapped (Default is True)

mask: Mask used for mapping data. For example, user can supply mask for specfic country. As of now, should be same
lat/lon grid as ETOPO data. (Default is None)

scale: Scale default size of colorbar labels (Default is 1.0)

show_cb: Boolean flag which defines whether colorbar is plotted on map (Default is False)

ticklabel_perc: Boolean flag which defines if ticklabels should have % at the end (Default is False)

diff: Boolean flag which defines if data shows difference of two datasets (Default is False)

cbar_ax: Array that defines location of colorbar. [x-position, y-position, width, height] (Default is [])



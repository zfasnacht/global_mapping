import matplotlib
matplotlib.use('Agg')
import pandas as pd
from scipy.interpolate import RegularGridInterpolator
import h5py
import sys
import numpy as np
from pylab import *
from global_mapping import map_data
import cartopy.crs as ccrs
from netCDF4 import Dataset 
import os
from scipy.ndimage import gaussian_filter

#############################################################################################################################
# 
# This code is a sample driver for the global_mapping library. Here we plot 2020 NO2, 2015-2019 NO2, and the difference 
# between the two. The code reads in a control file that includes user provided options such as region for mapping, 
# physical features to include on map, colorbar location and Colorbar mins/maxs. Code expects following inputs:
#
# python map_no2_subregion.py month, output_map_directory, control_filename
#  
#
#############################################################################################################################


def read_data(month):
    #Grabbing NO2 data, first grabbing 2020 NO2, then baseline NO2 for 2015-2019
    f = Dataset('OMNO2_Data/OMI_trno2_0.10x0.10_2020'+str(month).zfill(2)+'_Col3_V4.nc','r')
    no2_2020 = np.array(f['TroposphericNO2'][:])
    no2_2020[no2_2020==-1.2676506e+30]= np.nan

    x_len = len(no2_2020[:,0])
    y_len = len(no2_2020[0,:])
    lat = np.repeat(np.linspace(-90,90,x_len)[:,np.newaxis],y_len,-1)
    lon = np.repeat(np.linspace(-180,180,y_len)[np.newaxis,:],x_len,0)

    #Filling any small gaps in the data such as a single pixel here or there, likel having minimal impact as gaps are more than a single pixel 
    fn= RegularGridInterpolator((lat[:,0],lon[0,:]),no2_2020,bounds_error=False,fill_value= np.array([np.nan],dtype=np.float32)[0])
    no2_2020[np.isnan(no2_2020)] = fn((lat[np.isnan(no2_2020)],lon[np.isnan(no2_2020)]))
    no2_2020 = np.array(no2_2020,dtype=np.float32)

    #Applying a gaussian filter to smooth the NO2 data. Played around with a sigma and found Stand Dev * 1.3 to produce a clean result that 
    #is not overly smoothed 
    no2_2020 = gaussian_filter(no2_2020/(10e14),sigma=np.nanstd(no2_2020/(10e14))*1.3)


    base_sums = np.zeros_like(no2_2020)
    base_npt = np.zeros_like(no2_2020)

    #Grabbing 2015-2019 NO2 
    for year in range(2015,2020):
        f = Dataset('OMNO2_Data/OMI_trno2_0.10x0.10_'+str(year)+str(month).zfill(2)+'_Col3_V4.nc','r')

        no2 = np.array(f['TroposphericNO2'][:])
        no2[no2==-1.2676506e+30]= np.nan

        base_sums[~np.isnan(no2)] =  base_sums[~np.isnan(no2)]  + no2[~np.isnan(no2)]
        base_npt[~np.isnan(no2)] =  base_npt[~np.isnan(no2)]  + 1.

    no2_base = base_sums/base_npt

    #Interpolating over any small gaps, gaps are not likely in the 2015-2019 data since there are 5 years of data, but still have 
    #included this in case 
    fn= RegularGridInterpolator((lat[:,0],lon[0,:]),no2_base,bounds_error=False,fill_value= np.array([np.nan],dtype=np.float32)[0])
    no2_base[np.isnan(no2_base)] = fn((lat[np.isnan(no2_base)],lon[np.isnan(no2_base)]))
    no2_base = np.array(no2_base,dtype=np.float32)

    #Applying the same gaussian filter to the 2015-2019 data 
    no2_base = gaussian_filter(no2_base/(10e14),sigma=np.nanstd(no2_base/(10e14))*1.3)

    #Taking the difference of the smoothed datasets for the difference plot. Also looked at taking raw difference and smoothing, but 
    #for lower NO2 there is so much noise in the raw difference that smoothing it does not turn out as clean as just taking the 
    #difference of the smoothed results 
    no2_diff = (no2_2020) - (no2_base)

    
    return no2_2020, no2_base, no2_diff, lat, lon

#User can define month of interest, output directory for maps, and input text file with information on region desired for mapping 
month = int(sys.argv[1])
out_dir  = sys.argv[2]
inp_txt = sys.argv[3]

#Turning month into a string for titles and filenames 
month_str = datetime.datetime(2006,month,1).strftime("%B")


#Reading input control file which provides information on regions for maybe along with user requested options for plotting
#Maybe a more modern way of doing this? Meant to make running of code easy for users not as familiar with python
data = pd.read_csv(inp_txt)

#Reading OMNO2 data
no2_2020, no2_base, no2_diff, no2_lat, no2_lon = read_data(month)



for index, row in data.iterrows():


    #Checking to see if requested region has a sub-directory for saving plots, if not, will create it
    #Future updates may increase directory structure by adding subdirectories for month 
    dir_name = out_dir+'/'+row['Name'].replace(' ','')+'/'

    if not os.path.exists(dir_name):
        os.mkdir(dir_name)

    #Grabbing lat/lon min/max of region of interest. Will mask data outside of this region to increase efficiency (added 5 
    #degree pad in lat/lon to to make sure if cartopy expands plot, there will be no missing data regions)
    lat_min = row['LatMin'] ; lat_max = row['LatMax']
    lon_min = row['LonMin'] ; lon_max = row['LonMax']
    no2_inds = (no2_lat > (lat_max+5)) | (no2_lat < (lat_min-5)) | (no2_lon > (lon_max+5)) | (no2_lon < (lon_min-5))

    #Making temporary array for NO2, this way we can mask out the data outside bounding region as NaNs and still keep original NO2 
    #data for next iteration of loop
    plot_no2 = np.array(no2_2020)
    plot_no2[no2_inds] = np.nan
    
    #Making figure aspect ratio based on lon/lat range ratio, might want to find a way to standardize aspect ratio of plots by 
    #adjusting lon/lat bounds, possible future update?
    ratio = (lon_max-lon_min)/(lat_max-lat_min)

    #Making a figure with mercator projection. One note...have discovered oddities with non Plate-Caree projections. Using non
    #Plate Carree projections, cartopy seems to degrade the resolution. The current fix is using regrid_shape in plotting 
    #commands as shown in map_data library, but this is slow, so alternate fix would be beneficial. Switching to PlateCarree
    #avoids this problem, but is not as nice as of a projection for such work, so regrid_shape is current solution 
    fig, ax = plt.subplots(figsize=(10*ratio,10),subplot_kw=dict(projection=ccrs.Mercator(central_longitude=(lon_min+lon_max)/2.,min_latitude=lat_min,max_latitude=lat_max)))

    #Calling mapping library, full list of library features are in Readme document. Additional requested features can be added, 
    #so please send them along
    map_data(fig,ax,plot_no2,cb_label='NO$_2$ (10$^{15}$molecules/cm$^2$)',cb2_label = month_str+' 2020\nAverage',map_extent = [lon_min,lon_max,lat_min,lat_max],data_max=row['UpperBound'],provinces=row['Provinces'],roads=row['Roads'],plot_ocean=row['PlotOcean'],topo=row['Topo'],show_cb=True,cbar_ax=[row['cbar_x'],row['cbar_y'],row['cbar_width'],row['cbar_height']],scale=row['Scale'])

    #Saving the figure
    filename = dir_name+month_str+'_2020_NO2_'+row['Name'].replace(' ','')+'_Colorbar.png'
    plt.savefig(filename,dpi=300)
    plt.clf()
    plt.close()
    
    #Making temporary array for NO2, this way we can mask out the data outside bounding region as NaNs and still keep original NO2 
    #data for next iteration of loop
    plot_no2 = np.array(no2_base)
    plot_no2[no2_inds] = np.nan

    #Making figure aspect ratio based on lon/lat range ratio, might want to find a way to standardize aspect ratio of plots by 
    #adjusting lon/lat bounds, possible future update?    
    ratio = (lon_max-lon_min)/(lat_max-lat_min)

    #Making a figure with mercator projection. One note...have discovered oddities with non Plate-Caree projections. Using non
    #Plate Carree projections, cartopy seems to degrade the resolution. The current fix is using regrid_shape in plotting 
    #commands as shown in map_data library, but this is slow, so alternate fix would be beneficial. Switching to PlateCarree
    #avoids this problem, but is not as nice as of a projection for such work, so regrid_shape is current solution 
    fig, ax = plt.subplots(figsize=(10*ratio,10),subplot_kw=dict(projection=ccrs.Mercator(central_longitude=(lon_min+lon_max)/2.,min_latitude=lat_min,max_latitude=lat_max)))

    #Making a map with mapping library
    map_data(fig,ax,plot_no2,cb_label='NO$_2$ (10$^{15}$molecules/cm$^2$)',cb2_label = month_str+' 2015-2019\nAverage',map_extent = [lon_min,lon_max,lat_min,lat_max],data_max=row['UpperBound'],provinces=row['Provinces'],roads=row['Roads'],plot_ocean=row['PlotOcean'],topo=row['Topo'],show_cb=True,cbar_ax=[row['cbar_x'],row['cbar_y'],row['cbar_width'],row['cbar_height']],scale=row['Scale'])

    #Saving the figure
    filename = dir_name+month_str+'_2015_2019_NO2_'+row['Name'].replace(' ','')+'_Colorbar.png'
    plt.savefig(filename,dpi=300)
    plt.clf()
    plt.close()


    #Making temporary array for NO2, this way we can mask out the data outside bounding region as NaNs and still keep original NO2 
    #data for next iteration of loop
    plot_no2 = np.array(no2_diff)
    plot_no2[no2_inds] = np.nan

    #Making figure aspect ratio based on lon/lat range ratio, might want to find a way to standardize aspect ratio of plots by 
    #adjusting lon/lat bounds, possible future update?        
    ratio = (lon_max-lon_min)/(lat_max-lat_min)
    fig, ax = plt.subplots(figsize=(10*ratio,10),subplot_kw=dict(projection=ccrs.Mercator(central_longitude=(lon_min+lon_max)/2.,min_latitude=lat_min,max_latitude=lat_max)))


    #Making a figure with mercator projection. One note...have discovered oddities with non Plate-Caree projections. Using non
    #Plate Carree projections, cartopy seems to degrade the resolution. The current fix is using regrid_shape in plotting 
    #commands as shown in map_data library, but this is slow, so alternate fix would be beneficial. Switching to PlateCarree
    #avoids this problem, but is not as nice as of a projection for such work, so regrid_shape is current solution 
    map_data(fig,ax,plot_no2,cb_label='NO$_2$ (10$^{15}$molecules/cm$^2$)',cb2_label = 'Difference in '+month_str+'\n2020 vs. 2015-19',map_extent = [lon_min,lon_max,lat_min,lat_max],data_max=row['MaxDiff'],data_min=row['MinDiff'],colorbar='NO2Diff',provinces=row['Provinces'],roads=row['Roads'],plot_ocean=row['PlotOcean'],topo=row['Topo'],show_cb=True,diff=True,cbar_ax=[row['cbar_x'],row['cbar_y'],row['cbar_width'],row['cbar_height']],scale=row['Scale'])

    #Saving the figure
    filename = dir_name+month_str+'_2020_minus_2015_2019_NO2_'+row['Name'].replace(' ','')+'_Colorbar.png'
    plt.savefig(filename,dpi=300)
    plt.clf()
    plt.close()



    
